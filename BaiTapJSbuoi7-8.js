/******************************************************************************************************************* */
var NumberArray = [];

function demSo() {
  var numberEL = document.querySelector("#txt-number").value * 1;

  NumberArray.push(numberEL);

  document.querySelector("#txt-number").value = "";
  document.querySelector(".input-number").innerHTML = NumberArray;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 1 Tính tổng số dương
function tinhTongDuongtrongArr() {
  var tinhTongDuong = 0;

  for (var i = 0; i <= NumberArray.length; i++) {
    if (NumberArray[i] > 0) {
      tinhTongDuong += NumberArray[i];
    }
  }

  document.querySelector(".tinh-tong-duong").innerHTML = tinhTongDuong;
}

/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 2 Đếm số dương
function demSoDuongtrongArr() {
  var luuSoDuong = 0;

  for (var i = 0; i < NumberArray.length; i++) {
    if (NumberArray[i] > 0) {
      luuSoDuong++;
    }
  }

  document.querySelector(".dem-so-duong").innerHTML = luuSoDuong;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 3 tìm số nhỏ nhất
function TimSoNhoNhatTrongArr() {
  var min = 0;

  for (var i = 1; i < NumberArray.length; i++) {
    if (NumberArray[i] < min) {
      min = NumberArray[i];
    }
  }
  document.querySelector(".tim-so-nho-nhat").innerHTML = min;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 4 tìm số dương nhỏ nhất
function TimSoDuongNhoNhatTrongArr() {
  var minDuong = -1;

  for (var i = 0; i < NumberArray.length; i++) {
    if (minDuong == -1 || (minDuong > NumberArray[i] && NumberArray[i] > 0)) {
      minDuong = NumberArray[i];
    }
  }
  document.querySelector(".tim-so-duong-nho-nhat").innerHTML = minDuong;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 5 Tìm số chẵn cuối cùng trong mảng. Nếu mảng không có giá trị chẵn thì trả về -1.
function TimSoChanCuoiCungTrongArr() {
  var soLe = -1;

  for (var i = 0; i < NumberArray.length; i++) {
    if (NumberArray[i] % 2 == 0) {
      soLe = NumberArray[i];
    }
  }
  document.querySelector(".tim-so-chan-cuoi-cung-trongArr").innerHTML = soLe;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
// Bài 6 Đổi chỗ
function DoiCho() {
  var index1 = document.querySelector("#vi-tri-1").value * 1;
  var index2 = document.querySelector("#vi-tri-2").value * 1;

  var index;
  NumberArray[index] = NumberArray[index1];
  NumberArray[index] = NumberArray[index2];

  console.log(NumberArray[index1], NumberArray[index2]);

  var temp6 = NumberArray[index1];
  NumberArray[index1] = NumberArray[index2];
  NumberArray[index2] = temp6;

  document.querySelector(".doi-cho-trongArr").innerHTML = NumberArray;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
//Bài 7 sắp xếp tăng dần
function sapXepTangDan() {
  for (var i = 0; i < NumberArray.length; i++) {
    for (var j = i + 1; j < NumberArray.length; j++) {
      if (NumberArray[i] > NumberArray[j]) {
        var temp7 = NumberArray[i];
        NumberArray[i] = NumberArray[j];
        NumberArray[j] = temp7;
      }
    }
  }
  document.querySelector(".sap-xep-tang-dan").innerHTML = NumberArray;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
//Bài 8 tìm số nguyên tố
function timSoNguyenTo() {
  //kiểm tra số nguyên tố
  function checkSoNT(n) {
    var checkSoNT = 1;
    if (n < 2) {
      return (checkSoNT = -1);
    }
    for (var i = 2; i < n; i++) {
      if (i % n == 0) {
        return (checkSoNT = -1);
      }
    }
    return checkSoNT;
  }

  //lấy số nguyên tố trong mảng
  for (j = 0; j < NumberArray.length; j++) {
    if (checkSoNT(NumberArray[j] == 1)) {
      var soNT = NumberArray[j];
      break;
    }
  }
  document.querySelector(
    ".tim-so-nguyen-to"
  ).innerHTML = `số nguyên tố đầu tiên: ${soNT}`;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
//Bài 9 kiểm tra số nguyên
function checkSoNguyen() {
  var soNguyen = 0;
  for (var i = 0; i < NumberArray.length; i++) {
    if (Number.isInteger(NumberArray[i])) {
      soNguyen++;
    }
  }
  document.querySelector(
    ".dem-so-nguyen"
  ).innerHTML = `có ${soNguyen} số nguyên`;
}
/******************************************************************************************************************* */

/******************************************************************************************************************* */
//Bài 10 so sánh số
function soSanhSo() {
  var soDuong = 0;
  var soAm = 0;
  var result;
  for (var i = 0; i < NumberArray.length; i++) {
    if (NumberArray[i] > 0 && NumberArray[i] !== 0) {
      soDuong++;
    } else if (NumberArray[i] < 0 && NumberArray[i] !== 0) {
      soAm++;
    }

    if (soDuong > soAm) {
      result = `số dương nhiều hơn số âm`;
    } else if (soAm > soDuong) {
      result = `số âm nhiều hơn số dương`;
    } else {
      result = `số dương bằng số âm`;
    }
  }
  document.querySelector(
    ".so-sanh-so"
  ).innerHTML = `có ${soDuong} số dương , ${soAm} số âm ==> ${result}`;
}
/******************************************************************************************************************* */
